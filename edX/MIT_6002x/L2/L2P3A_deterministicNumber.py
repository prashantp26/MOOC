import random
def deterministicNumber():
    '''
    Deterministically generates and returns an even number between 9 and 21
    '''
    # Your code here
    return random.choice(range(20,9,-2))

print deterministicNumber()