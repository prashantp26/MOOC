import pylab
def genPrimes():

    primes = [2]
    x = 2
    yield 2
    while True:
        x += 1
        prime = True
        for p in primes:            
            if x%p == 0:
                prime = False
                break
        if prime:                
            primes.append(x)                
            yield x


def plot(x,y):
    
    pylab.figure(1) #create figure 1
    pylab.plot(x,y) 
    pylab.title('Prime numbers')
    pylab.xlabel('Nth prime')
    pylab.ylabel('Prime')
    pylab.show()
        

##################################
primeGenerator = genPrimes()

n = 5000
i = 0

x = []
y = []

while i < n:
    p = primeGenerator.next()
    x.append(i)
    y.append(p)
    #print p
    i += 1

plot(x,y)