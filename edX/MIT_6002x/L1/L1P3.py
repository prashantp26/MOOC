import pylab

def loadTuples():

	inFile = open('julyTemps.txt', 'r')
	high = []
	low = []

	for line in inFile:
		fields = line.split()

		if not ( len(fields) != 3 or 'Boston' == fields[0] or 'Day' == fields[0] ):
			high.append(int(fields[1]))
			low.append(int(fields[2]))

	return (low,high)

def producePlot(lowTemps, highTemps):
	diffTemps = []

	for i, temp in enumerate(lowTemps):
		diffTemps.insert(i, highTemps[i] - lowTemps[i])

	# use numpy to manipulate lists:
	# diffTemps = list(np.array(highTemps) - np.array(lowTemps))
	
	pylab.figure(1) #create figure 1
	pylab.plot(range(1,32), diffTemps) 
	pylab.title('Day by Day Ranges in Temperature in Boston in July 2012')
	pylab.xlabel('Days')
	pylab.ylabel('Temperature Ranges')
	pylab.show()
		
(low,high) = loadTuples()
producePlot(low,high)