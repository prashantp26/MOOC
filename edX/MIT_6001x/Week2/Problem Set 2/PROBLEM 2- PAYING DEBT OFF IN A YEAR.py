balance = 3926
annualInterestRate = 0.2

monthlyInterestRate = annualInterestRate/12

def report():

	monthlyInterest = 0
	unpaidBalance = 0
	minimumMonthlyPayment = 0

	while unpaidBalance >= 0:
		totalPaid = 0
		unpaidBalance = balance
		minimumMonthlyPayment += 10		

		for monthNumber in range(1,13):
			if unpaidBalance <= 0:
				break					

			unpaidBalance -= minimumMonthlyPayment 
			monthlyInterest = monthlyInterestRate * unpaidBalance
			unpaidBalance += monthlyInterest
			totalPaid = totalPaid + minimumMonthlyPayment

	print("Lowest Payment: " + str(minimumMonthlyPayment))
report()
