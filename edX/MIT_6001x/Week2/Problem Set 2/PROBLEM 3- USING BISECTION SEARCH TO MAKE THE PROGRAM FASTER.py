balance = 999999
annualInterestRate = .18

monthlyInterestRate = annualInterestRate/12

def getUnpaidBalance(guess): # guess = guessed minimumMonthlyPayment 

	unpaidBalance = balance
	minimumMonthlyPayment = guess		

	for monthNumber in range(1,13):

		unpaidBalance 		-=	minimumMonthlyPayment 				#minimumMonthlyPayment = guess 
		monthlyInterest 	=	monthlyInterestRate * unpaidBalance
		unpaidBalance 		+=	monthlyInterest

	return unpaidBalance

def report():

	lower = balance/12.0
	upper = (balance*((1+monthlyInterestRate)**12))/12
	epsilon = 0.01 # accuracy needed is to the cent
	guess = ((upper-lower)/2.0)+lower	

	unpaid = getUnpaidBalance(guess)

	while abs(unpaid) > epsilon:

		if abs(unpaid) <= epsilon:
			break
		elif unpaid > 0:
			lower = guess
		else:
			upper = guess

		guess = ((upper-lower)/2.0)+lower	

		unpaid = getUnpaidBalance(guess)

	print("Lowest Payment: " + str(round(guess,2)))

report()
