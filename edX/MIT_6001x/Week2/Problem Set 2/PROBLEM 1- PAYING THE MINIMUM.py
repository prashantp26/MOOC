balance = 4842
annualInterestRate = 0.2
monthlyPaymentRate = 0.04

monthlyInterestRate = annualInterestRate/12

def report():

	monthlyInterest = 0
	unpaidBalance = balance
	totalPaid = 0

	for monthNumber in range(1,13):

		minimumMonthlyPayment = monthlyPaymentRate * unpaidBalance
		unpaidBalance -= minimumMonthlyPayment 

		monthlyInterest = monthlyInterestRate * unpaidBalance

		unpaidBalance += monthlyInterest

		totalPaid = totalPaid + minimumMonthlyPayment

		print("Month: " + str(monthNumber))
 		print("Minimum monthly payment: " + str(round(minimumMonthlyPayment,2)))
 		print("Remaining balance: " + str(round(unpaidBalance,2)))

	print("Total paid: " + str(round(totalPaid,2)))
	print("Remaining balance: " + str(round(unpaidBalance,2)))		


report()
