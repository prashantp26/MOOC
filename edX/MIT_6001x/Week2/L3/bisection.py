low = 0
high = 100
guess = (high-low)/2
print("Please think of a number between 0 and 100!")
print("Is your secret number " + str(guess) +" ?")
#print("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly.")
response = raw_input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly.")

while response != "c":
	
	if response == "h":
		high = guess
		guess = (high-low)/2 + low
		print("Is your secret number " + str(guess) +" ?")
		response = raw_input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly.")
	
	elif response == "l":
		low = guess
		guess = (high-low)/2 + low
		print("Is your secret number " + str(guess) +" ?")
		response = raw_input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly.")
	
	else:
		print("Sorry, I did not understand your input.")
		print("Is your secret number " + str(guess) +" ?")
		response = raw_input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly.")

print("Game over. Your secret number was: " +str(guess))
