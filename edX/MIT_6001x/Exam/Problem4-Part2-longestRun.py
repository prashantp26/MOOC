def longestRun(L):
    
	run = [L[0]]
	i = 0
	maxList = run[:]

	while i < len(L)-1:
		if L[i+1] >= L[i]:
			run.append(L[i+1])
			if len(run) >= len(maxList):
				maxList = run[:]
		else:
			del run[:]
			run = [L[i+1]]	
		i += 1
		
	return len(maxList)	

L = [10, 4, 6, 8, 3, 4, 5, 7, 7, 2]
print longestRun(L)