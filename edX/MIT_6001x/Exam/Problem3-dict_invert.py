def dict_invert(d):
    '''
    d: dict
    Returns an inverted dictionary according to the instructions above
    '''
    # Your code here
    inv = {}
    for key,item in d.iteritems():
        inv[item] = inv.get(item,[])
        inv[item].append(key)
        inv.get(item).sort()
    return inv

d1 = {1:10, 2:20, 3:30}
d2 = {1:10, 2:20, 3:30, 4:30}
d3 = {4:True, 2:True, 0:True}
d4 = {8: 6, 2: 6, 4: 6, 6: 6}

print dict_invert(d1)
print dict_invert(d2)
print dict_invert(d3)
print dict_invert(d4)
