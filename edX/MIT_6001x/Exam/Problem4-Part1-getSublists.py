def getSublists(L, n):

	start = 0
	sublists = []

	while start <= len(L)-n:
		sublists.append(L[start:(start+n)])
		start += 1

	return sublists
		
L = [10, 4, 6, 8, 3, 4, 5, 7, 7, 2]
n = 4

print getSublists(L,n)