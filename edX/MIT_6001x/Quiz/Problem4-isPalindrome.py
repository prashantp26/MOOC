def isPalindrome(aString):
    '''
    aString: a string
    '''
    # Your code here
    if len(aString) <= 1:
        return True
    elif aString[0] != aString[-1]:
        return False
    else:                
        return isPalindrome(aString[1:-1])        

print(isPalindrome('1111'))