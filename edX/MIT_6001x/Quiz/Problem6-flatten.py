def flatten(aList):
    ''' 
    aList: a list 
    Returns a copy of aList, which is a flattened version of aList 
    '''
    flat = []

    for item in aList:
    	if not type(item) is list:
    		flat.append(item)
    	else:
    		flat += flatten(item)
    return flat


print flatten([[1,'a',['cat'],2],[[[3]],'dog'],4,5])