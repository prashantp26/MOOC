def dict_interdiff(d1, d2):
    '''
    d1, d2: dicts whose keys and values are integers
    Returns a tuple of dictionaries according to the instructions above
    '''
    # Your code here

    intersect = {}
    difference = {}

    #intersect:
    for i in d1.keys():
        if i in d2.keys():
            intersect[i] = f(d1[i], d2[i])

    #difference:
    for i in d1.keys():
        if not i in d2.keys():
            difference[i] = d1[i]

    for i in d2.keys():
        if not i in d1.keys():
            difference[i] = d2[i]
 
    return (intersect, difference)                    


def f(a, b):
    return (a + b)

d1 = {1:30, 2:20, 3:30, 5:80}
d2 = {1:40, 2:50, 3:60, 4:70, 6:90}

print dict_interdiff(d1, d2)