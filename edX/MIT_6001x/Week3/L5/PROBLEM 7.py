def lenRecur(aStr):
    '''
    aStr: a string
    
    returns: int, the length of aStr
    '''
    # Your code here
    if aStr == "":
    	return 0
    else:
    	aStr = aStr[0:-1]
    	return 1+lenRecur(aStr)	

print lenRecur('helloworld')