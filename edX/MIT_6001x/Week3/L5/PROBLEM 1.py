def iterPower(base, exp):
    '''
    base: int or float.
    exp: int >= 0
 
    returns: int or float, base^exp
    '''
    # Your code here
    b = 1.0000
    while exp > 0:
        b = b * base
        exp -= 1
    return b 
       

iterPower(-7.74, 0)