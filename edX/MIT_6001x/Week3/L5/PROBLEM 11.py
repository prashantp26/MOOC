animals = { 'a': ['aardvark'], 'b': ['baboon'], 'c': ['coati']}

animals['d'] = ['donkey']
animals['d'].append('dog')
animals['d'].append('dingo')

def biggest(aDict):
    '''
    aDict: A dictionary, where all the values are lists.

    returns: The key with the largest number of values associated with it
    '''
    # Your Code Here

    biggestSize = 0
    biggestKey = None
    for key in aDict:
    	if len(aDict[key]) >= biggestSize:
    		biggestSize = len(aDict[key])
    		biggestKey = key
    return biggestKey

print(biggest({'A': []}))        