def isIn(char, aStr):
    '''
    char: a single character
    aStr: an alphabetized string
    
    returns: True if char is in aStr; False otherwise
    '''
    # Your code here
    if len(aStr) == 0:
        return False

    middle = len(aStr)/2 # int middle value

    if aStr[middle] == char or aStr[-1] == char or aStr[0] == char:
        return True
    elif char > aStr[middle]:
        aStr = aStr[middle:-1]
    else:
        aStr = aStr[1:middle] 

    return isIn(char, aStr) 

print(isIn("2", "abcdef"))       
