def recurPowerNew(base, exp):
    '''
    base: int or float.
    exp: int >= 0

    returns: int or float; base^exp
    '''
    # Your code here
    b = base/1.0

    if exp <= 0:
    	return 1
    elif exp%2 == 0: #even
    	e = exp/2
    	return recurPowerNew((b*b), e)
    else:	
    	return b * recurPowerNew(b, exp-1)

print(recurPowerNew(32,7))	



	