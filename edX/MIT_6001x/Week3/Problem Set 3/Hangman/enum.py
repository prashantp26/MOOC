import string

def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE...
    alphabetList = []
    for c in string.ascii_lowercase:
        alphabetList.append(c)
    a2z = alphabetList[:]

    for c in lettersGuessed:
        if c in a2z:
            alphabetList.remove(c)
    return "".join(alphabetList)

print(getAvailableLetters(['e', 'i', 'k', 'p', 'r', 's']))