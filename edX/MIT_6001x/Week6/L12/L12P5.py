def genPrimes():

    primes = [2]
    x = 2
    yield 2
    while True:
        x += 1
        prime = True
        for p in primes:            
            if x%p == 0:
                prime = False
                break
        if prime:                
            primes.append(x)                
            yield x

primeGenerator = genPrimes()

n = 10000
i = 0

while i < n:
    print primeGenerator.next()    
    i += 1