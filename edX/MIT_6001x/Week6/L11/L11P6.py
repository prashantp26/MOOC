class Queue(object):
    """A FIFO data structure that and insert and remove elements"""

    def __init__(self):
        """Create and empty queue using list data type"""
        self.vals = []

    def insert(self, e):
        """Add an element to end of the structure""" 
        self.vals.append(e)

    def remove(self):
        """POP an element from the front of the structure"""
        try:
            return self.vals.pop(0)
        except:
            raise ValueError('not found')
 
    def __str__(self):
        """Returns a string representation of self"""
        string = ""
        for i in self.vals:
            string += str(i)+" "
        return string            

    def __len__(s):
        return len(s.vals) 

queue = Queue()
queue.insert(5)
queue.insert(6)
queue.insert(7)
print len(queue)
print queue
queue.remove()
print queue
queue.remove()
